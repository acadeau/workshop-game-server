
// Affichage de la page d'accueil
function home(){

  // Ajout d'un handler pour l'évenement click. Celui ci lancera la partie si tout est bon
  // TO DO : Ajouter les verifs.
  document.getElementById('play').addEventListener("click", function(e){
    let formP1 = Array.apply(null, document.getElementById("p1").elements);
    let formP2 = Array.apply(null, document.getElementById("p2").elements);

    console.log(formP1);

    let inputToDict = function(dict, element) {
      dict[element.name] = element.value;
      return dict;
    }

    let p1 = formP1.reduce(inputToDict, {});
    let p2 = formP2.reduce(inputToDict, {});

    start(p1, p2);

  });

}

// Charge le template du jeu
// Il peut être interessant de récuperer l'HTML avec un requête ajax pour le charger dans section.view
function start(p1, p2){
  localStorage.setItem("playerName1",p1.name);
  localStorage.setItem("playerName2",p2.name);
  localStorage.setItem("playerIp1",p1.ip);
  localStorage.setItem("playerIp2",p2.ip);
}

home();